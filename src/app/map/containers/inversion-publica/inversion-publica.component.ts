import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {NgProgress, NgProgressRef} from '@ngx-progressbar/core';
import '../../../../../node_modules/leaflet-extra-markers/dist/js/leaflet.extra-markers.js';
import {geoJSON, icon, latLng, latLngBounds, LatLngBounds, Layer, layerGroup, Map, Marker, tileLayer} from "leaflet";
import {GuatemalaMunicipioService} from "../../services/guatemala-municipio.service";

@Component({
  selector: 'app-inversion-publica',
  templateUrl: './inversion-publica.component.html',
  styleUrls: ['./inversion-publica.component.css']
})
export class InversionPublicaComponent implements OnInit {

    public guatemalaMunicipioData: any;

    public allMunicipioCelularUseGeojson: any;
    public allMunicipioInternetUseGeojson: any;
    public allMunicipioUnder30UseGeojson: any;

    public styleCelularUseMap: any;
    public styleInternetUseMap: any;
    public styleUnder30UseMap: any;

    public progressRef: NgProgressRef;

    // Define our base layers so we can reference them multiple times
    public streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    public wMaps = tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
        detectRetina: true,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    public options = {
        layers: [],
        zoom: 9,
        center: latLng([ 46.879966, -121.726909 ])
    };

    public layers: Layer[] = [];

    public layersControl = {
        baseLayers: {
        },
        overlays: {
        }
    };

    public fitBounds: LatLngBounds;

    constructor(
      private readonly guatemalaMunicipioService: GuatemalaMunicipioService,
      private progress: NgProgress,
      private changeDetector: ChangeDetectorRef) {
    }

    ngOnInit(): void {
      this.progressRef = this.progress.ref('myProgress');
      this.setStylesAndColor();
      this.guatemalaMunicipioService.getAll().subscribe(data => {
        this.guatemalaMunicipioData = data;
        this.loadGuatemalaMap();
      });
    }

    setStylesAndColor() {
        this.styleCelularUseMap = (feature: any) => {
            return {
                fillColor: feature.properties.COLOR_USO_CELULAR,
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.9
            };
        };

        this.styleInternetUseMap = (feature: any) => {
            return {
                fillColor: feature.properties.COLOR_USO_INTERNET,
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.9
            };
        };

        this.styleUnder30UseMap = (feature: any) => {
            return {
                fillColor: feature.properties.COLOR_MENORES_30,
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.9
            };
        };
    }

    loadGuatemalaMap() {

        this.allMunicipioCelularUseGeojson = geoJSON(this.guatemalaMunicipioData, { style: this.styleCelularUseMap});
        this.allMunicipioInternetUseGeojson = geoJSON(this.guatemalaMunicipioData, { style: this.styleInternetUseMap});
        this.allMunicipioUnder30UseGeojson = geoJSON(this.guatemalaMunicipioData, { style: this.styleUnder30UseMap});

       // const normalRegion = layerGroup([this.streetMaps, this.allMunicipioCelularUseGeojson]);
        //const citiesRegion = layerGroup([this.wMaps, this.allMunicipioCelularUseGeojson]);

        this.layersControl.baseLayers['Normal'] = this.streetMaps;
        this.layersControl.baseLayers['Ciudades'] = this.wMaps;
        this.layersControl.overlays['USO DE CELULAR'] = this.allMunicipioCelularUseGeojson;
        this.layersControl.overlays['USO DE INTERNET'] = this.allMunicipioInternetUseGeojson;
        this.layersControl.overlays['MENORES DE 30'] = this.allMunicipioUnder30UseGeojson;
        this.layers.push(this.streetMaps);
    }

    onMapReady(map: Map) {
        const iconRetinaUrl = 'assets/marker-icon-2x.png';
        const iconUrl = 'assets/marker-icon.png';
        const shadowUrl = 'assets/marker-shadow.png';
        const iconDefault = icon({
            iconRetinaUrl,
            iconUrl,
            shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41]
        });
        Marker.prototype.options.icon = iconDefault;


        const southWest = latLng(12, -87);
        const northEast = latLng(17.5, -94.7);
        const bounds = latLngBounds(southWest, northEast);

        map.fitBounds(bounds);
        map.setMaxZoom(11);
        map.setMinZoom(8);
        map.setView([15.891640645706688,  -90.69783592224123], 8);
        map.zoomControl.remove();
        map.dragging.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
    }
}
