import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { InversionPublicaComponent } from './containers/inversion-publica/inversion-publica.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import {GuatemalaMunicipioService} from "./services/guatemala-municipio.service";




@NgModule({
  declarations: [InversionPublicaComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    LeafletModule
  ],
  providers: [
    GuatemalaMunicipioService
  ]
})
export class MapModule { }
