import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class GuatemalaMunicipioService {

  constructor(protected readonly http: HttpClient) { }
 
  getAll(): Observable<any> {
    return this.http.get('/assets/geojson/new_municipios_gt.json');
  }
}
